
# Conceptos básicos de Markdown
Acontinuación se describen los conceptos básicos de la creación en Markdown.

# Encabezados
Para crear un encabezado, utilice el símbolo de almohadilla (#) al principio de una línea:
```
Ejemplo :
```
# This is level 1 (article title)
## This is level 2
### This is level 3
#### This is level 4
##### This is level 5


# Texto básico
En Markdown, un párrafo no requiere sintaxis especial.
Para aplicar negrita al texto, se escribe entre dos asteriscos. Para aplicar cursiva al texto, se escribe entre un solo asterisco:
```
Ejemplo :
```
This text is **bold**.
This text is *italic*.
This text is both ***bold and italic***.

Para ignorar los caracteres de formato de Markdown, ponga \ antes del carácter:
```
Ejemplo :
```
This is not \*italicized\* type.
This is not *italicized* type.


# Listas numeradas y listas con viñetas
Para crear listas numeradas, empiece una línea con 1. or 1), pero no mezcle los formatos dentro de la misma lista. No es necesario especificar los números. GitHub lo hace por usted.
```
Ejemplo :
```
1. This is step 1.
1. This is the next step.
1. This is yet another step, the third.

Para crear listas de viñetas, empiece una línea con *, - o +, pero no mezcle los formatos dentro de la misma lista. (No mezcle formatos de viñetas, como * y +, dentro del mismo documento).
```
Ejemplo :
```
* First item in an unordered list.
* Another item.
* Here we go again.

También puede incrustar listas dentro de listas y añadir contenido entre elementos de la lista.
```
Ejemplo :
```
1. Set up your table and code blocks.
1. Perform this step.

   ![screen](https://seminariosescuela.com/wp-content/uploads/2020/08/siga-modificar.png)

1. Make sure that your table looks like this:

   | Hello | World |
   |---|---|
   | How | are you? |

1. This is the fourth step.

   >[!NOTE]
   >
   >This is note text.

1. Do another step.

# Tablas
Las tablas no forman parte de la especificación principal de Markdown, pero Adobe las admite en cierta medida. Markdown no admite listas de líneas múltiples en celdas. La práctica recomendada es evitar el uso de varias líneas en las tablas. Puede crear tablas utilizando la barra vertical (|) para definir columnas y filas. Los guiones crean el encabezado de cada columna, mientras que las barras verticales separan las columnas. Incluya una línea en blanco antes de la tabla para que se muestre correctamente.
```
Ejemplo :
```
| Header | Another header | Yet another header |
|--- |--- |--- |
| row 1 | column 2 | column 3 |
| row 2 | row 2 column 2 | row 2 column 3 |

# Vínculos
La sintaxis Markdown para un vínculo en línea consiste en la parte [link text], que es el texto que se va a hipervincular, seguido de la parte (file-name.md), que es la URL o el nombre de archivo al que se va a vincular:

[link text](file-name.md)
```
Ejemplo :
```
[Ministerio de Economía y Finanzas](https://www.mef.gob.pe)

# Imágenes
```
Ejemplo :
```

![MEF Logo](https://www.centroliber.pe/sites/default/files/2023-02/ARCHIVO%20LIBER%20-%20MINISTERIO%20DE%20ECONOMI%CC%81A.png)

# Bloques de código
Markdown admite la colocación de bloques de código tanto en línea como en un bloque “delimitado” independiente entre frases.

Utilice comillas invertidas (`) para crear estilos de código en línea dentro de un párrafo. Para crear un bloque de código multilínea específico, agregue tres comillas invertidas ( `) antes y después del bloque de código (denominado “bloque de código delimitado” en Markdown y “componente de bloque de código” en AEM). Para bloques de código delimitado, agregue el lenguaje del código después del primer conjunto de comillas invertidas para que Markdown resalte correctamente la sintaxis del código. Ejemplo: ` javascript

```
Ejemplo :
```
This is `inline code` within a paragraph of text.

Este es un bloque de código delimitado:
```
Ejemplo :
```
function test() {
 console.log("notice the blank line before this function?");


# Extensiones de Markdown personalizadas

# Bloques de notas
Puede elegir entre estos tipos de bloques de notas para llamar la atención sobre un contenido específico:

[!NOTE]
[!TIP]
[!IMPORTANT]
[!CAUTION]
[!WARNING]
[!ADMINISTRATION]
[!AVAILABILITY]
[!PREREQUISITES]
[!ERROR]
[!ADMINISTRATION]
[!INFO]
[!SUCCESS]

En general, los bloques de notas deben usarse con moderación porque pueden resultar molestos. Aunque también se admiten bloques de código, imágenes, listas y vínculos, intente que los bloques de notas sean simples y directos.
```
Ejemplo :
```
>[!NOTE]
>
>This is a standard NOTE block.

>[!TIP]
>
>This is a standard TIP.

>[!IMPORTANT]
>
>This is an IMPORTANT note.
